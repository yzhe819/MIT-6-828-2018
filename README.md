# MIT-6.828

Notes, labs, materials for MIT 6.828



### Sections:

The major parts of the JOS operating system are:

1. Booting  
2. Memory management  
3. User environments  
4. Preemptive multitasking  
5. File system, spawn, and shell  
6. Network driver



### Reference materials:

Source: [6.828: Learning by doing](https://pdos.csail.mit.edu/6.828/2018/overview.html)

Git book: [xv6 - a simple, Unix-like teaching operating system](https://th0ar.gitbooks.io/xv6-chinese/content/)

xv6 Chinese document: [xv6 中文文档](https://github.com/ranxian/xv6-chinese)

Darwin Kernel (mirror): [XNU kernel](https://github.com/apple/darwin-xnu)

Reference lab note: 

- [woai3c - MIT 6.828](https://github.com/woai3c/MIT6.828)
- [solidcc2 - mit-6.828-note](https://github.com/solidcc2/mit-6.828-note)
- [clann24 - jos](https://github.com/clann24/jos)
- [MIT 6.828 JOS 操作系统学习笔记](https://www.cnblogs.com/fatsheep9146/category/769143.html)




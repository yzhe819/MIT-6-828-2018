# Lab 1: Booting a PC

[[toc]]

## Introduction

This lab is split into three parts. The first part concentrates on getting familiarized with x86 assembly language, the QEMU x86 emulator, and the PC's power-on bootstrap procedure. The second part examines the boot loader for our 6.828 kernel, which resides in the `boot` directory of the `lab` tree. Finally, the third part delves into the initial template for our 6.828 kernel itself, named JOS, which resides in the `kernel` directory.

## Part 1: PC Bootstrap

### Simulating the x86

```bash
cd lab
make
```

![image-20210730144252401](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210730144252401.png)

Now we are ready to run QEMU, supplying the file `obj/kern/kernel.img`, created above, as the contents of the emulated PC's "virtual hard disk." This hard disk image contains both our boot loader (`obj/boot/boot`) and our kernel (`obj/kernel`)

I am using the WSL2 to run this lab, so use the serial console without the virtual VGA by running make qemu-nox.

```bash
make qemu-nox
```

![image-20210730143954793](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210730143954793.png)

To quit qemu, type **Ctrl+a x**.

There are only two commands can be used by the kernel monitor, `help` and `kerninfo`.

> Although simple, it's important to note that this kernel monitor is running "directly" on the "raw (virtual) hardware" of the simulated PC. This means that you should be able to copy the contents of `obj/kern/kernel.img` onto the first few sectors of a _real_ hard disk, insert that hard disk into a real PC, turn it on, and see exactly the same thing on the PC's real screen as you did above in the QEMU window.

![image-20210730231208404](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210730231208404.png)

### The PC's Physical Address Space

A PC's physical address space is hard-wired to have the following general layout:

```
            +------------------+  <- 0xFFFFFFFF (4GB)
            |      32-bit      |
            |  memory mapped   |
            |     devices      |
            |                  |
            /\/\/\/\/\/\/\/\/\/\

            /\/\/\/\/\/\/\/\/\/\
            |                  |
            |      Unused      |
            |                  |
            +------------------+  <- depends on amount of RAM
            |                  |
            |                  |
            | Extended Memory  |
            |                  |
            |                  |
            +------------------+  <- 0x00100000 (1MB)
            |     BIOS ROM     |
            +------------------+  <- 0x000F0000 (960KB)
            |  16-bit devices, |
            |  expansion ROMs  |
            +------------------+  <- 0x000C0000 (768KB)
            |   VGA Display    |
            +------------------+  <- 0x000A0000 (640KB)
            |                  |
            |    Low Memory    |
            |                  |
            +------------------+  <- 0x00000000
```

#### 8088 processor

The first PCs, which were based on the 16-bit Intel 8088 processor, were only capable of addressing **1MB** of physical memory (from 0x00000000 to 0x000FFFFF instead of 0xFFFFFFFF). The **640KB** area marked "Low Memory" was the _only_ random-access memory (RAM) that an early PC could use.

Following it, the **384KB** area from 0x000A0000 through 0x000FFFFF was reserved by the hardware for special uses such as video display buffers and firmware held in non-volatile memory. The most important part of this reserved area is the Basic Input/Output System (BIOS), which occupies the 64KB region from 0x000F0000 through 0x000FFFFF. In early PCs the BIOS was held in true read-only memory (ROM), but current PCs store the BIOS in updateable flash memory.

> The BIOS is responsible for performing basic system initialization such as activating the video card and checking the amount of memory installed. After performing this initialization, the BIOS loads the operating system from some appropriate location such as hard disk or the network, and passes control of the machine to the operating system.

#### 80286 and 80386 processors

80286 and 80386 processors supported **16MB** and **4GB** physical address spaces respectively. But still preserved the original layout for the low 1MB of physical address space in order to ensure backward compatibility with existing software.

> Modern PCs therefore have a "hole" in physical memory from 0x000A0000 to 0x00100000, dividing RAM into "low" or "conventional memory" (the first 640KB) and "extended memory" (everything else).

### The ROM BIOS

> Open two terminal windows and cd both shells into your lab directory. In one, enter make qemu-gdb (or make qemu-nox-gdb). This starts up QEMU, but QEMU stops just before the processor executes the first instruction and waits for a debugging connection from GDB. In the second terminal, from the same directory you ran `make`, run make gdb.

One terminal show:

![image-20210730233556779](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210730233556779.png)

Another one show:

![image-20210730233540314](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210730233540314.png)

Based on the above screenshot, we can clearly see:

```assembly
[f000:fff0] 0xffff0:	ljmp   $0xf000,$0xe05b
```

which is the GDB's disassembly of the first instruction to be executed.

- Therefore, we can find that PC starts executing at physical address 0x000FFFF0, which is at the very top of the 64KB area reserved for the ROM BIOS (0x000FFFF0 just a litter lower than 0x00100000).
- The PC starts executing with `CS = 0xf000` and `IP = 0xfff0`
- The first instruction to be executed is a `jmp` instruction, which jumps to the segmented address `CS = 0xf000` and `IP = 0xe05b`.

The reason of this is that the BIOS in a PC is "hard-wired" to the physical address range 0x000f0000-0x000fffff, this design ensures that the BIOS always gets control of the machine first after power-up or any system restart - which is crucial because on power-up there _is_ no other software anywhere in the machine's RAM that the processor could execute. The QEMU emulator comes with its own BIOS, which it places at this location in the processor's simulated physical address space. On processor reset, the (simulated) processor enters real mode and sets CS to 0xf000 and the IP to 0xfff0, so that execution begins at that (CS:IP) segment address.

In real mode (the mode that PC starts off in), address translation works according to the formula:

$$
physical\ address = 16 * segment + offset
$$

So, when the PC sets CS to 0xf000 and IP to 0xfff0, the physical address referenced is:

```
   16 * 0xf000 + 0xfff0   # in hex multiplication by 16 is
   = 0xf0000 + 0xfff0     # easy--just append a 0.
   = 0xffff0
```

`0xffff0` is 16 bytes before the end of the BIOS (`0x100000`). Therefore we shouldn't be surprised that the first thing that the BIOS does is `jmp` backwards to an earlier location in the BIOS for the following operation.

## Part 2: The Boot Loader

Floppy and hard disks for PCs are divided into 512 byte regions called _sectors_. A sector is the disk's minimum transfer granularity: each read or write operation must be one or more sectors in size and aligned on a sector boundary. If the disk is bootable, the first sector is called the _boot sector_ (index 0), since this is where the boot loader code resides. When the BIOS finds a bootable floppy or hard disk, it loads the 512-byte boot sector into memory at physical addresses 0x7c00 through 0x7dff, and then uses a `jmp` instruction to set the CS:IP to `0000:7c00`, passing control to the boot loader. Like the BIOS load address, these addresses are fairly arbitrary - but they are fixed and standardized for PCs.

> For 6.828, however, we will use the conventional hard drive boot mechanism, which means that our boot loader must fit into a measly 512 bytes. The boot loader consists of one assembly language source file, `boot/boot.S`, and one C source file, `boot/main.c` Look through these source files carefully and make sure you understand what's going on. The boot loader must perform two main functions:
>
> 1. First, the boot loader switches the processor from real mode to _32-bit protected mode_, because it is only in this mode that software can access all the memory above 1MB in the processor's physical address space. Protected mode is described briefly in sections 1.2.7 and 1.2.8 of [PC Assembly Language](https://pdos.csail.mit.edu/6.828/2018/readings/pcasm-book.pdf), and in great detail in the Intel architecture manuals. At this point you only have to understand that translation of segmented addresses (segment:offset pairs) into physical addresses happens differently in protected mode, and that after the transition offsets are 32 bits instead of 16.
>
> 2. Second, the boot loader reads the kernel from the hard disk by directly accessing the IDE disk device registers via the x86's special I/O instructions. If you would like to understand better what the particular I/O instructions here mean, check out the "IDE hard drive controller" section on [the 6.828 reference page](https://pdos.csail.mit.edu/6.828/2018/reference.html). You will not need to learn much about programming specific devices in this class: writing device drivers is in practice a very important part of OS development, but from a conceptual or architectural viewpoint it is also one of the least interesting.

let check the boot.S first. boot.S will use the cli to disable interrupt then internalize the segment registers (DS, ES, SS) to zero.

```assembly
start:
  .code16                     # Assemble for 16-bit mode
  cli                         # Disable interrupts
  cld                         # String operations increment

  # Set up the important data segment registers (DS, ES, SS).
  xorw    %ax,%ax             # Segment number zero
  movw    %ax,%ds             # -> Data Segment
  movw    %ax,%es             # -> Extra Segment
  movw    %ax,%ss             # -> Stack Segment
```

Then, it will turn to enable the A20 (the 21st physical address line). For backwards compatibility with the earliest PCs (8086 processor, which only have 20 physical address lines), physical address line 20 is tied low, so that addresses higher than 1MB wrap (0xFFFFF) around to zero (0x00000) by default. When the time turn to 80286 processor, for backwards compatibility, the A20 will be closed by default. So, we need to enable A20.

```assembly
  # Enable A20:
  #   For backwards compatibility with the earliest PCs, physical
  #   address line 20 is tied low, so that addresses higher than
  #   1MB wrap around to zero by default.  This code undoes this.
seta20.1:
  inb     $0x64,%al               # Wait for not busy
  testb   $0x2,%al
  jnz     seta20.1

  movb    $0xd1,%al               # 0xd1 -> port 0x64
  outb    %al,$0x64

seta20.2:
  inb     $0x64,%al               # Wait for not busy
  testb   $0x2,%al
  jnz     seta20.2

  movb    $0xdf,%al               # 0xdf -> port 0x60
  outb    %al,$0x60
```

Next, **lgdt** is used to load the **gdtdesc** - Global Descriptor Table description (6 bytes long) into Global Descriptor Table Register. The parameter of **lgdt** is a 48-bit-long memory range.

```assembly
  # Switch from real to protected mode, using a bootstrap GDT
  # and segment translation that makes virtual addresses
  # identical to their physical addresses, so that the
  # effective memory map does not change during the switch.
  lgdt    gdtdesc
  movl    %cr0, %eax
  orl     $CR0_PE_ON, %eax
  movl    %eax, %cr0
```

The content of gdtdesc can be seen from the following codes. The lower 16 bit is set the size of GDT and the following 32 bits is the address of GDT. Also, we can see the consistence of gdt, which are null segment , code segment and data segment. The start addresses are all 0x0 and Segment boundaries are 0xffffffff.

```assembly
gdt:
  SEG_NULL				# null seg
  SEG(STA_X|STA_R, 0x0, 0xffffffff)	# code seg
  SEG(STA_W, 0x0, 0xffffffff)	        # data seg

gdtdesc:
  .word   0x17                            # sizeof(gdt) - 1
  .long   gdt                             # address gdt
```

Following the lgdt is setting the control register cr0, which turn into the protected model from real model. Then, it will switch the processor into 32-bit mode and set up the protected mode data segment register, then jump to set the esp and call the bootmain (which is defined in main.c)

```assembly
# Jump to next instruction, but in 32-bit code segment.
  # Switches processor into 32-bit mode.
  ljmp    $PROT_MODE_CSEG, $protcseg

  .code32                     # Assemble for 32-bit mode
protcseg:
  # Set up the protected-mode data segment registers
  movw    $PROT_MODE_DSEG, %ax    # Our data segment selector
  movw    %ax, %ds                # -> DS: Data Segment
  movw    %ax, %es                # -> ES: Extra Segment
  movw    %ax, %fs                # -> FS
  movw    %ax, %gs                # -> GS
  movw    %ax, %ss                # -> SS: Stack Segment

  # Set up the stack pointer and call into C.
  movl    $start, %esp
  call bootmain
```

Turn to the bootmain: It will read the 1st page from the disk to physical memory address 0x10000 and check whether it is a valid ELF file (by verifying the magic number). The size of page is decided by eight sectors, which are the disk's minimum transfer granularities. If it is not a valid ELF file, it will jump to bad section and do nothing. Otherwise, it will load each program segments. The readseg(uint32_t pa, uint32_t count, uint32_t offset) method will read 'count' bytes at 'offset' from kernel into physical address 'pa'. Therefore, it will read the program segment from ph->p_offset into loading address ph->p_pa. Once the loading finished, jump to the entry point ELFHDR->e_entry of the ELF file.

```c
#define ELFHDR		((struct Elf *) 0x10000) // scratch space
void
bootmain(void)
{
	struct Proghdr *ph, *eph;

	// read 1st page off disk
	readseg((uint32_t) ELFHDR, SECTSIZE*8, 0);

	// is this a valid ELF?
	if (ELFHDR->e_magic != ELF_MAGIC)
		goto bad;

	// load each program segment (ignores ph flags)
	ph = (struct Proghdr *) ((uint8_t *) ELFHDR + ELFHDR->e_phoff);
	eph = ph + ELFHDR->e_phnum;
	for (; ph < eph; ph++)
		// p_pa is the load address of this segment (as well
		// as the physical address)
		readseg(ph->p_pa, ph->p_memsz, ph->p_offset);

	// call the entry point from the ELF header
	// note: does not return!
	((void (*)(void)) (ELFHDR->e_entry))();

bad:
	outw(0x8A00, 0x8A00);
	outw(0x8A00, 0x8E00);
	while (1)
		/* do nothing */;
}
```

### Loading the Kernel

> An ELF binary starts with a fixed-length _ELF header_, followed by a variable-length _program header_ listing each of the program sections to be loaded. The C definitions for these ELF headers are in `inc/elf.h`. The program sections we're interested in are:
>
> - `.text`: The program's executable instructions.
> - `.rodata`: Read-only data, such as ASCII string constants produced by the C compiler. (We will not bother setting up the hardware to prohibit writing, however.)
> - `.data`: The data section holds the program's initialized data, such as global variables declared with initializers like `int x = 5;`.

Examine the full list of the names, sizes, and link addresses of all the sections in the kernel executable by typing:

```bash
objdump -h obj/kern/kernel
```

![image-20210731175237165](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731175237165.png)

You will see many more sections than the ones we listed above, but the others are not important for our purposes. Most of the others are to hold debugging information, which is typically included in the program's executable file but not loaded into memory by the program loader.

Take particular note of the "VMA" (or _link address_) and the "LMA" (or _load address_) of the `.text` section. The load address of a section is the memory address at which that section should be loaded into memory. The link address of a section is the memory address from which the section expects to execute. (It is possible to generate _position-independent_ code that does not contain any such absolute addresses.) Typically, the link and load addresses are the same.

The boot loader uses the ELF _program headers_ to decide how to load the sections. The program headers specify which parts of the ELF object to load into memory and the destination address each should occupy.

```bash
objdump -x obj/kern/kernel
```

![image-20210731175958813](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731175958813.png)

The program headers are then listed under "Program Headers" in the output of objdump. The areas of the ELF object that need to be loaded into memory are those that are marked as "LOAD". Other information for each program header is given, such as the virtual address ("vaddr"), the physical address ("paddr"), and the size of the loaded area ("memsz" and "filesz").

## Part 3: The Kernel

### Using virtual memory to work around position dependence

Operating system kernels often like to be linked and run at very high _virtual address_, such as 0xf0100000, in order to leave the lower part of the processor's virtual address space for user programs to use. The reason for this arrangement will become clearer in the next lab. Many machines don't have any physical memory at address 0xf0100000, so we can't count on being able to store the kernel there. Instead, we will use the processor's memory management hardware to map virtual address 0xf0100000 (the link address at which the kernel code _expects_ to run) to physical address 0x00100000 (where the boot loader loaded the kernel into physical memory). This way, although the kernel's virtual address is high enough to leave plenty of address space for user processes, it will be loaded in physical memory at the 1MB point in the PC's RAM, just above the BIOS ROM. This approach requires that the PC have at least a few megabytes of physical memory

Up until `kern/entry.S` sets the `CR0_PG` flag, memory references are treated as physical addresses (strictly speaking, they're linear addresses, but boot/boot.S set up an identity mapping from linear addresses to physical addresses and we're never going to change that). Once `CR0_PG` is set, memory references are virtual addresses that get translated by the virtual memory hardware to physical addresses. `entry_pgdir` translates virtual addresses in the range 0xf0000000 through 0xf0400000 to physical addresses 0x00000000 through 0x00400000, as well as virtual addresses 0x00000000 through 0x00400000 to physical addresses 0x00000000 through 0x00400000.

# Lab 1 Exercise Answer:

## Exercise 1:

Getting Started with x86 assembly

![image-20210731000430595](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731000430595.png)

- [PC Assembly Language Book](https://pdos.csail.mit.edu/6.828/2018/readings/pcasm-book.pdf)
- [Brennan's Guide to Inline Assembly](http://www.delorie.com/djgpp/doc/brennan/brennan_att_inline_djgpp.html)
- [the 6.828 reference page](https://pdos.csail.mit.edu/6.828/2018/reference.html)
- [汇编语言 - 王爽](https://book.douban.com/subject/25726019/)

## Exercise 2:

![image-20210731000508392](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731000508392.png)

```
(gdb) si
[f000:e05b]    0xfe05b: cmpl   $0x0,%cs:0x6ac8
0x0000e05b in ?? ()
(gdb) si
[f000:e062]    0xfe062: jne    0xfd2e1
0x0000e062 in ?? ()
(gdb) si
[f000:e066]    0xfe066: xor    %dx,%dx
0x0000e066 in ?? ()
(gdb) si
[f000:e068]    0xfe068: mov    %dx,%ss
0x0000e068 in ?? ()
(gdb) si
[f000:e06a]    0xfe06a: mov    $0x7000,%esp
0x0000e06a in ?? ()
(gdb) si
[f000:e070]    0xfe070: mov    $0xf34c2,%edx
0x0000e070 in ?? ()
(gdb) si
[f000:e076]    0xfe076: jmp    0xfd15c
0x0000e076 in ?? ()
(gdb) si
[f000:d15c]    0xfd15c: mov    %eax,%ecx
0x0000d15c in ?? ()
(gdb) si
[f000:d15f]    0xfd15f: cli
0x0000d15f in ?? ()
(gdb) si
[f000:d160]    0xfd160: cld
0x0000d160 in ?? ()
(gdb) si
[f000:d161]    0xfd161: mov    $0x8f,%eax
0x0000d161 in ?? ()
(gdb) si
[f000:d167]    0xfd167: out    %al,$0x70
0x0000d167 in ?? ()
(gdb) si
[f000:d169]    0xfd169: in     $0x71,%al
0x0000d169 in ?? ()
(gdb) si
[f000:d16b]    0xfd16b: in     $0x92,%al
0x0000d16b in ?? ()
(gdb) si
[f000:d16d]    0xfd16d: or     $0x2,%al
0x0000d16d in ?? ()
(gdb) si
[f000:d16f]    0xfd16f: out    %al,$0x92
0x0000d16f in ?? ()
(gdb) si
[f000:d171]    0xfd171: lidtw  %cs:0x6ab8
0x0000d171 in ?? ()
(gdb) si
[f000:d177]    0xfd177: lgdtw  %cs:0x6a74
0x0000d177 in ?? ()
(gdb) si
[f000:d17d]    0xfd17d: mov    %cr0,%eax
0x0000d17d in ?? ()
(gdb) si
[f000:d180]    0xfd180: or     $0x1,%eax
0x0000d180 in ?? ()
(gdb) si
[f000:d184]    0xfd184: mov    %eax,%cr0
0x0000d184 in ?? ()
(gdb) si
[f000:d187]    0xfd187: ljmpl  $0x8,$0xfd18f
0x0000d187 in ?? ()
(gdb) si
The target architecture is assumed to be i386
=> 0xfd18f:     mov    $0x10,%eax
0x000fd18f in ?? ()
```

## Exercise 3:

![image-20210731164934723](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731164934723.png)

The b \*0x7c00 sets a breakpoint at address 0x7C00, then using the c to continue execution until the next breakpoint, also use x/Ni ADDR, where N is the number of consecutive instructions to disassemble and ADDR is the memory address at which to start disassembling.

![image-20210801151206321](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210801151206321.png)

Answer the following questions:

- At what point does the processor start executing 32-bit code? What exactly causes the switch from 16- to 32-bit mode?

  Answer: The `ljmp $PROT_MODE_CSEG, $protcseg` causes the switch from 16- to 32-bit mode in the `boot.S`

  ```assembly
  # Jump to next instruction, but in 32-bit code segment.
  # Switches processor into 32-bit mode.
  ljmp    $PROT_MODE_CSEG, $protcseg
  ```

- What is the _last_ instruction of the boot loader executed, and what is the _first_ instruction of the kernel it just loaded?

  The last instruction of the boot loader executed is:

  ```c
  ((void (*)(void)) (ELFHDR->e_entry))();
  ```

  And the first instruction of the kernel it just loaded is:

  ```
  f010000c:	66 c7 05 72 04 00 00 	movw   $0x1234,0x472
  ```

- _Where_ is the first instruction of the kernel?

  Since the last instruction the boot loader executed is `call *0x10018`

  ```assembly
  7d63:	ff 15 18 00 01 00    	call   *0x10018
  ```

  Therefore, the address of first instruction of the kernel is at \*0x10018.

  ![image-20210731173713098](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731173713098.png)

- How does the boot loader decide how many sectors it must read in order to fetch the entire kernel from disk? Where does it find this information?

  The boot loader reads the number the `program header`s in the `ELF header` and loads them all:

  ```c
    ph = (struct Proghdr *) ((uint8_t *) ELFHDR + ELFHDR->e_phoff);
    eph = ph + ELFHDR->e_phnum;
    for (; ph < eph; ph++)
      readseg(ph->p_pa, ph->p_memsz, ph->p_offset);
  ```

## Exercise 4:

![image-20210731174151678](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731174151678.png)

The running result:

![image-20210731174655969](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731174655969.png)

## Exercise 5:

The BIOS loads the boot sector into memory starting at address 0x7c00, so this is the boot sector's load address. This is also where the boot sector executes from, so this is also its link address. We set the link address by passing `-Ttext 0x7C00` to the linker in `boot/Makefrag`, so the linker will produce the correct memory addresses in the generated code.

![image-20210731180514239](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731180514239.png)

change the value to 0x7e00

<img src="https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731181051026.png" alt="image-20210731181051026" style="zoom:80%;" />

then, recompile:

<img src="https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731180843095.png" alt="image-20210731180843095" style="zoom: 67%;" />

the boot.asm will be changed to

![image-20210731181334483](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731181334483.png)

compared with the original one:

<img src="https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731172747784.png" alt="image-20210731172747784" style="zoom:100%;" />

The starting address has changed to 0x7e00 now. So, the first boot loader's link wrong occured at:

```
0x7c1e:      lgdtw  0x7e64
```

From previous note, lgdt is used to load the Global Descriptor Table description from 0x7e64 into Global Descriptor Table Register. The original address should be 0x7c64, so the GDT register only received 48 zero bits, then cause a error. The following screenshot show the data content of 0x7e64 and 0x7c64, respectively.

![image-20210731231823097](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731231823097.png)

## Exercise 6:

Besides the section information, there is one more field in the ELF header that is important to us, named `e_entry`. This field holds the link address of the _entry point_ in the program: the memory address in the program's text section at which the program should begin executing. You can see the entry point:

```
objdump -f obj/kern/kernel
```

![image-20210731233933236](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731233933236.png)

You should now be able to understand the minimal ELF loader in `boot/main.c`. It reads each section of the kernel from disk into memory at the section's load address and then jumps to the kernel's entry point.

![image-20210731181651561](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731181651561.png)

The state of memory at 0x00100000 at the point the BIOS enters the boot loader

![image-20210731234517653](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731234517653.png)

At the point the boot loader enters the kernel

![image-20210731235036393](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731235036393.png)

From the kernel.asm, which set the memory state of 0x00100000

![image-20210731235612087](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731235612087.png)

## Exercise 7:

![image-20210731181816168](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731181816168.png)Therefore, we set the breakpoint at 0x10000C, then run until we find the

```assembly
movl    %eax, %cr0
```

![image-20210801142416678](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210801142416678.png)

the single step over that instruction by using stepi (the same thing as si)

![image-20210801142530008](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210801142530008.png)

The data at 0x00100000 and 0xf0100000 is the same now. The value is mapped from 0x00100000 to 0xf0100000.

After we comment out this instruction in kern/entry.S, re-make and run again.

![image-20210801145115442](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210801145115442.png)

therefore the first instruction will fail to work is

```assembly
0x10002a:    jmp    *%eax
```

Without the `CR0_PG` setting, memory references are virtual addresses that will not be translated by the virtual memory hardware to physical addresses. And machines don't have any physical memory at address 0xf010000c. Therefore, when the system jump to 0xf010000c, which is trying execute code outside RAM or ROM.

![image-20210801145318934](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210801145318934.png)

## Exercise 8:

Most people take functions like `printf()` for granted, sometimes even thinking of them as "primitives" of the C language. But in an OS kernel, we have to implement all I/O ourselves.

Read through `kern/printf.c`, `lib/printfmt.c`, and `kern/console.c`, and make sure you understand their relationship. It will become clear in later labs why `printfmt.c` is located in the separate `lib` directory.

![image-20210731181840340](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210731181840340.png)

Be able to answer the following questions:

1. Explain the interface between `printf.c` and `console.c`. Specifically, what function does `console.c` export? How is this function used by `printf.c`?

2. Explain the following from console.c:

   ```
   if (crt_pos >= CRT_SIZE) {
   	int i;
   	memmove(crt_buf, crt_buf + CRT_COLS, (CRT_SIZE - CRT_COLS) * sizeof(uint16_t));
   		for (i = CRT_SIZE - CRT_COLS; i < CRT_SIZE; i++)
   			crt_buf[i] = 0x0700 | ' ';
   		crt_pos -= CRT_COLS;
   }
   ```

   

3. For the following questions you might wish to consult the notes for Lecture 2. These notes cover GCC's calling convention on the x86.

   Trace the execution of the following code step-by-step:

   ```
   int x = 1, y = 3, z = 4;
   cprintf("x %d, y %x, z %d\n", x, y, z);
   ```

   - In the call to `cprintf()`, to what does `fmt` point? To what does `ap` point?
   - List (in order of execution) each call to `cons_putc`, `va_arg`, and `vcprintf`. For `cons_putc`, list its argument as well. For `va_arg`, list what `ap` points to before and after the call. For `vcprintf` list the values of its two arguments.

4. Run the following code.

   ```
   unsigned int i = 0x00646c72;
   cprintf("H%x Wo%s", 57616, &i);
   ```

   What is the output? Explain how this output is arrived at in the step-by-step manner of the previous exercise.

   [Here's an ASCII table](http://web.cs.mun.ca/~michael/c/ascii-table.html) that maps bytes to characters.

   The output depends on that fact that the x86 is little-endian. If the x86 were instead big-endian what would you set `i` to in order to yield the same output? Would you need to change `57616` to a different value?

   [Here's a description of little- and big-endian](http://www.webopedia.com/TERM/b/big_endian.html) and [a more whimsical description](http://www.networksorcery.com/enp/ien/ien137.txt).

5. In the following code, what is going to be printed after 'y=' ? (note: the answer is not a specific value.) Why does this happen?

   ```
   cprintf("x=%d y=%d", 3);
   ```

6. Let's say that GCC changed its calling convention so that it pushed arguments on the stack in declaration order, so that the last argument is pushed last. How would you have to change `cprintf` or its interface so that it would still be possible to pass it a variable number of arguments?

   ![image-20210801000526190](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210801000526190.png)

## Exercise 9:

![image-20210801000558061](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210801000558061.png)

## Exercise 10:

![image-20210801000701777](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210801000701777.png)

## Exercise 11:

![image-20210801000718171](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210801000718171.png)

## Exercise 12:

![image-20210801000749038](https://github.com/yzhe819/lab/raw/lab-1/wiki/img/image-20210801000749038.png)

## Reference material:

[exercise 5](https://www.cnblogs.com/fatsheep9146/p/5220004.html)